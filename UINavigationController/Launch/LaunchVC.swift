//
//  ViewController.swift
//  UINavigationController
//
//  Created by Nheng Vanchhay on 25/11/21.
//

import UIKit

class LaunchVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    private let myArray: NSArray = [ "RootViewController"
                                    ,"PopToRootViewController"
                                    ,"PopToViewController & PopToViewController",
                                     "PresentingViewController",
                                     "SetViewController"]
    private var myTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let barHeight: CGFloat = UIApplication.shared.statusBarFrame.size.height
        let displayWidth: CGFloat = self.view.frame.width
        let displayHeight: CGFloat = self.view.frame.height
        
        myTableView = UITableView(frame: CGRect(x: 0, y: barHeight, width: displayWidth, height: displayHeight - barHeight))
        myTableView.register(UITableViewCell.self, forCellReuseIdentifier: "MyCell")
        myTableView.dataSource = self
        myTableView.delegate = self
        self.view.addSubview(myTableView)    }
    
    func getVC(sbName: String, vcName: String) -> UIViewController{
        // Get SB name
        let sb         = UIStoryboard(name: sbName, bundle: nil)
        // Get VC name
        let vc         = sb.instantiateViewController(withIdentifier: vcName)
        return vc
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let vc = getVC(sbName: "RootViewControllerSB", vcName: "RootViewControllerVC")
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if indexPath.row == 1 {
            let vc = getVC(sbName: "PopToRootSB", vcName: "PopToRootVC")
            self.navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 2 {
            let vc = getVC(sbName: "PopViewControllerPopToViewControllerSB", vcName: "LoginVC")
            self.navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 3 {
            let vc = getVC(sbName: "PresentingViewControllerSB", vcName: "VC1")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        }else if indexPath.row == 4 {
            let vc = getVC(sbName: "SetViewController", vcName: "SetVC1")
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath as IndexPath)
        cell.textLabel!.text = "\(myArray[indexPath.row])"
        return cell
    }
    
    @IBAction func btnGo(_ sender: Any) {
    }
}

