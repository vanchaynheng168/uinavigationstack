//
//  RootViewControllerVC.swift
//  UINavigationController
//
//  Created by Nheng Vanchhay on 30/11/21.
//

import UIKit

class RootViewControllerVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func LoginAction(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "MainRootVC")
        self.navigationController?.pushViewController(vc!, animated: true)
        
        
        let navLoginVC   = storyboard?.instantiateViewController(withIdentifier: "NavMainRootVC")
        if let navViewController = navLoginVC as? UINavigationController {
            // Set root to Navigation of NavLoginVC
            if let window = UIApplication.shared.keyWindow {
                 window.rootViewController = navViewController
            }
        }
    }
}

class MainRootVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
}

class ListRootVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
}

