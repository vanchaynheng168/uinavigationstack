//
//  PopToViewVC.swift
//  UINavigationController
//
//  Created by Nheng Vanchhay on 29/11/21.
//

import UIKit

class PopToRootVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func mainAction(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ListVC")
        self.navigationController?.pushViewController(vc!, animated: true)
    }
}


class ListVC: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func listAction(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "DetailVC")
        self.navigationController?.pushViewController(vc!, animated: true)    }
}

class DetailVC: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()

        let newBackButton = UIBarButtonItem(title: "Back To Root", style: .plain, target: self, action: #selector(back(_:)))
        self.navigationItem.leftBarButtonItem = newBackButton
        
    }
    
    @objc func back(_ sender: UIBarButtonItem) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    @IBAction func DetailAction(_ sender: Any) {
        
    }
}
