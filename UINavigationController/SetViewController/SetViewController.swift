//
//  SetViewController.swift
//  UINavigationController
//
//  Created by Nheng Vanchhay on 29/11/21.
//

import UIKit

class SetViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func vc1Action(_ sender: Any) {
        
        // Declare navigationController
        var controllers = navigationController?.viewControllers
        
        // Check VC is available
        guard let secondVC = storyboard?.instantiateViewController(withIdentifier: "SetVC2") else {return}
        guard let thirdVC  = storyboard?.instantiateViewController(withIdentifier: "SetVC3") else {return}
        
        // Append VC intro navigationController
        controllers?.append(secondVC)
        controllers?.append(thirdVC)
        
        if let controllers = controllers {
            // This method will push or present depending on whether the new top view controller was previously in the stack.
            self.navigationController?.setViewControllers(controllers, animated: true)
        }
    }
}

class SetVC2: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(#function)
        // Do any additional setup after loading the view.
    }
}

class SetVC3: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
}
