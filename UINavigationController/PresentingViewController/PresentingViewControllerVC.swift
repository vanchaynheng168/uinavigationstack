//
//  PresentingViewControllerVC.swift
//  UINavigationController
//
//  Created by Nheng Vanchhay on 29/11/21.
//

import UIKit

class PresentingViewControllerVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

}

class VC4: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func vc4Action(_ sender: Any) {
        self.presentingViewController?.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
    }
}
