//
//  PopViewControllerPopToViewControllerVC.swift
//  UINavigationController
//
//  Created by Nheng Vanchhay on 29/11/21.
//

import UIKit

class PopViewControllerPopToViewControllerVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
}

class LoginVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func LoginAction(_ sender: Any) {
        
        //*******(1)*******
//        let mainVC = storyboard?.instantiateViewController(withIdentifier: "MainVC")
//        //get navVC of MainVC
//        let navVC   = storyboard?.instantiateViewController(withIdentifier: "navMainVC")
//        self.navigationController?.pushViewController(mainVC!, animated: true)
//        if let navViewController = navVC as? UINavigationController {
//            // Set root to Navigation of MainVC
//            if let window = UIApplication.shared.keyWindow {
//                 window.rootViewController = navViewController
//                print("navigation stack: ", self.navigationController?.viewControllers as Any)
//            }
//        }
        
        
        //*******(2)*******
//        let mainVC = storyboard?.instantiateViewController(withIdentifier: "MainVC")
//        self.navigationController?.pushViewController(mainVC!, animated: true)
        
        
        
//        *******(3)*******
        let navMainVC = storyboard?.instantiateViewController(withIdentifier: "navMainVC")
        navMainVC?.modalPresentationStyle = .fullScreen
        self.present(navMainVC!, animated: true, completion: nil)
    }
}

class MainVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func MainAction(_ sender: Any) {
        let sb = UIStoryboard(name: "PopViewControllerPopToViewControllerSB", bundle: nil)
        
        //*******(1 & 2 & 3)*******
        let logoutVC      = sb.instantiateViewController(withIdentifier: "LogoutVC")
        self.navigationController?.pushViewController(logoutVC, animated: true)
    }
}

class LogoutVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func LogoutAction(_ sender: Any) {
        
        //crash
//        let sb = UIStoryboard(name: "PopViewControllerPopToViewControllerSB", bundle: nil)
//        let vc = sb.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
//        self.navigationController?.popToViewController(vc, animated: true)
        
        
        print("navigation stack: ",self.navigationController?.viewControllers.description)
//        if let vc = self.navigationController?.viewControllers.filter({ $0 is LoginVC }).first as? LoginVC
//        {
//            self.navigationController?.popToViewController(vc, animated: true)
//        }
        

        
//        if let vc = self.navigationController?.viewControllers.last(where: { $0.isKind(of: LoginVC.self)}) as? LoginVC
//        {
//            self.navigationController?.popToViewController(vc, animated: true)
//        }
        
        
        
        //*******(1 & 3)*******
        let navLoginVC   = storyboard?.instantiateViewController(withIdentifier: "NavLoginVC")
        if let navViewController = navLoginVC as? UINavigationController {

            // Set root to Navigation of NavLoginVC
            if let window = UIApplication.shared.keyWindow {
                 window.rootViewController = navViewController
                print("navigation stack: ", self.navigationController?.viewControllers)
            }
        }
        
        
        //*******(1 & 2 & 3)*******
        if let vc = self.navigationController?.viewControllers.first(where: { $0.isKind(of: LoginVC.self)}) as? LoginVC
        {
            self.navigationController?.popToViewController(vc, animated: true)
        }
        
        //pop to previous view controller
        //self.navigationController?.popViewController(animated: true)
    }
}
